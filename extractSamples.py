from mdb import mdb
import numpy as np
import argparse

def filterData(X,Y,labels):
    """filter the features X and corresponding labels Y for given
    list of labels"""

    if not X.shape[0] == Y.shape[0]:
        raise ValueError('Number of features and labels should be same')
    #filter mask to select data for given list of  labels
    mask = np.zeros(Y.shape[0],dtype=bool)
    for label in labels:
        temp =  (Y == label)
        mask = np.logical_or(mask,temp)
    X = X[mask]
    Y = Y[mask]
    return X,Y

if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('-db', '--database', required=True,help="name of the database")
    parser.add_argument('-c', '--collection', required=True,help="name of the collection in database")
    args = vars(parser.parse_args())
    db = args['database']
    c = args['collection']
    data = mdb(db,c)
    print "Extratcting labelwise sample information from",db,"database and",c,"collection"
    labelsCount = data.labelsCount()
    print "Total Number of classes: ",labelsCount.shape[0]
    slabels = [] #selected labels
    for i in range(labelsCount.shape[0]):
        if labelsCount[i][1] >= 25:
            slabels.append(labelsCount[i][0])

    print "number of classes with number of samples more than 25:",len(slabels)
    labels = np.savetxt('labels.txt',np.asarray(slabels,dtype=int),fmt='%d')

    print "Extracting training Data .."
    xtr,ytr = data.getTrainData()
    #select data from only labels with frequecy more than 25
    try:
        xtr,ytr = filterData(xtr,ytr,slabels)
    except ValueError,e:
        print str(e)
    print "number of training examples: ",ytr.shape[0]
    print "saving training Features in xtr.gz"
    np.savetxt('xtr.gz',xtr,delimiter=",")
    print "saving corresponding training labels in ytr.gz"
    np.savetxt('ytr.gz',ytr,fmt='%d')
    
    print "Extracting testing Data .."
    xt,yt = data.getTestData()
    #select data from only labels with frequecy more than 25
    try:
        xt,yt = filterData(xt,yt,slabels)
    except ValueError,e:
        print str(e)
    print "number of testing samples: ",yt.shape[0]
    print "saving testing Features in xt.gz"
    np.savetxt('xt.gz',xt,delimiter=",")
    print "saving corresponding testing labels in yt.gz"
    np.savetxt('yt.gz',yt,fmt='%d')

