import numpy as np
from sklearn import svm
from sklearn.externals import joblib
from sklearn.cross_validation import StratifiedKFold,KFold
from sklearn.metrics import classification_report
from sklearn.grid_search import GridSearchCV
import os

print "loading training data.."
xtr = np.loadtxt('xtr.gz', dtype=float, delimiter=",")
ytr = np.loadtxt('ytr.gz', dtype=int, delimiter=",");

print "loaded",ytr.shape[0],"samples"

#using stratifiedKfold for equal distribution of samples per class
# in each folds
skf = StratifiedKFold(ytr, 3) #TODO:5 fold 

print skf

params ={'C': np.logspace(-2,1,4)} #TODO:'class_weight':[None,'balanced'] 
print "performing grid search for best parameters for ",params
svc = svm.LinearSVC()
clf = GridSearchCV(svc, params, cv = skf, n_jobs=2,verbose = 5,refit=True)
#TODO:try with scoring=f1-weighted, use n_jobs = -1 to use all threads
clf.fit(xtr, ytr)

print clf
print clf.best_params_

for params, mean_score, scores in clf.grid_scores_:
    print "%0.3f (+/-%0.03f) for %r" % (mean_score, scores.std() * 2, params)

if not os.path.isdir('./models'):
    os.makedirs('./models/')

joblib.dump(clf.best_estimator_, './models/clf.pkl')
yp = clf.predict(xtr)
print "Number of mislabeled points in training data : %d" % (ytr != yp).sum()
print "number of correct labeled points in training: %d" %(ytr == yp).sum()


print clf.score(xtr, ytr)

print "detailed Report:"
print classification_report(ytr, yp)
