import numpy as np
from sklearn import svm
from sklearn.externals import joblib
from sklearn.metrics import confusion_matrix
import argparse
import os
import matplotlib.pyplot as plt
from sklearn.metrics import classification_report

def plot_confusion_matrix(cm, labels,title='Confusion matrix', cmap=plt.cm.Blues):
        plt.imshow(cm, interpolation='nearest', cmap=cmap)
        plt.title(title)
        plt.colorbar()
        tick_marks = np.arange(len(labels))
        plt.xticks(tick_marks, labels, rotation=45)
        plt.yticks(tick_marks, labels)
        plt.tight_layout()
        plt.ylabel('True label')
        plt.xlabel('Predicted label')


if __name__=='__main__':
    
    parser = argparse.ArgumentParser()
    parser.add_argument('-m', '--model', required=True,help="classification model file path")
    args = vars(parser.parse_args())
    model = args['model']
    if not os.path.exists(model):
        print "Model file doesn't exists"
        exit()
    
    print "Loading the classification model"
    clf = joblib.load(model)
    print "Following classification model has been loaded"
    print clf

    print "loading testing data"
    xt = np.loadtxt('xt.gz', dtype=float, delimiter=",")
    yt = np.loadtxt('yt.gz', dtype=int, delimiter=",")

    print "number of testing Samples:",yt.shape[0]
    yp = clf.predict(xt)

    tp = (yt==yp).sum() # true prediction
    fp = (yt!=yp).sum() # false predictions
    tpn = float(tp)/(float(tp)+float(fp)) #true predictions normalized
    fpn = float(fp)/(float(tp)+float(fp))

    print "number of correct labeled points: "+ str(tp)+ " out of "+str(yt.shape[0]) + " i.e. " + str(tpn) + " percent."
    print "number of correct labeled points: "+ str(fp)+ " out of "+str(yt.shape[0]) + " i.e. " + str(fpn) + " percent."
    
    print "detailed Report:"
    print classification_report(yt,yp)

    # plotting confusion matrix
    #finalLabels = np.loadtxt('labels.txt',dtype=int)
    #cm = confusion_matrix(yt,yp)
    #np.set_printoptions(precision=3,threshold=np.nan)
    #print('Confusion matrix, without normalization')
    #print(cm)
    #plt.figure()
    #plot_confusion_matrix(cm,finalLabels)


    #cm_normalized = cm.astype('float') / cm.sum(axis=1)[:, np.newaxis]
    #print('Normalized confusion matrix')
    #print(cm_normalized)
    #plt.figure()
    #plot_confusion_matrix(cm_normalized,finalLabels, title='Normalized confusion matrix')

    #plt.show()
