from pymongo import MongoClient
import numpy as np
from bson.code import Code

class mdb:

    __client = MongoClient()

    def __init__(self,dbstr,cstr):
        self.__db = mdb.__client[dbstr]
        self.__colltn = self.__db[cstr]

    def getTrainData(self):
        nSamples = self.__colltn.find({'traintest01':0}).count()
        X = np.zeros((nSamples,500),dtype=float)
        Y = np.zeros(nSamples,dtype=int)
        i = 0
        curser = self.__colltn.find({'traintest01':0})
        for c in curser:

            X[i] = np.asarray(c['shp'].split(),dtype=float)
            Y[i] = np.asarray(c['subcato'],dtype=int)
            i = i+1
        return X,Y

    def getTestData(self):
        nSamples = self.__colltn.find({'traintest01':1}).count()
        X = np.zeros((nSamples,500),dtype=float)
        Y = np.zeros(nSamples,dtype=int)
        i = 0
        curser = self.__colltn.find({'traintest01':1})
        for c in curser:
            X[i] = np.asarray(c['shp'].split(),dtype=float)
            Y[i] = np.asarray(c['subcato'],dtype=int)
            i = i+1
        return X,Y

    def labelsCount(self):
        labels = []
        counts = []
        key = [ 'subcato' ]
        condition = { 'traintest01' : {'$gt': -1 }}
        initial = {'count' : 0}
        reduce = 'function(doc, out) { out.count++ }'
        labelsData = self.__colltn.group(key, condition, initial, reduce)
        for data in labelsData:
                labels.append(int(data['subcato']))
                counts.append(int(data['count']))
        return np.asarray((labels, counts)).T

